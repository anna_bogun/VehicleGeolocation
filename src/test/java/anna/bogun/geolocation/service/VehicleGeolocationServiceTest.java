package anna.bogun.geolocation.service;

import anna.bogun.geolocation.dto.Coordinates;
import anna.bogun.geolocation.dto.RectangleAreaDto;
import anna.bogun.geolocation.dto.VehicleGeolocationDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grum.geocalc.BoundingArea;
import com.grum.geocalc.Coordinate;
import com.grum.geocalc.Point;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VehicleGeolocationServiceTest {

    private VehicleGeolocationDto vehicleGeolocationDto;

    private RectangleAreaDto rectangleAreaDto;

    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        objectMapper = new ObjectMapper();

        vehicleGeolocationDto = new VehicleGeolocationDto("testname",
                new Coordinates(10.312, 56.432));

        rectangleAreaDto = new RectangleAreaDto(new Coordinates(70, 145),
                new Coordinates(50, 110));
    }

    @Test
    public void printJsonForManualTesting() throws JsonProcessingException {
        System.out.println(objectMapper.writeValueAsString(vehicleGeolocationDto));
        System.out.println(objectMapper.writeValueAsString(new VehicleGeolocationDto("testname1", new Coordinates(60, 120))));
        System.out.println(objectMapper.writeValueAsString(new VehicleGeolocationDto("testname2", new Coordinates(45, 120))));
        System.out.println(objectMapper.writeValueAsString(rectangleAreaDto));

    }

    @Test
    public void testBoundingAreaContains() {

        Point northEast = Point.at(Coordinate.fromDegrees(70), Coordinate.fromDegrees(145));
        Point southWest = Point.at(Coordinate.fromDegrees(50), Coordinate.fromDegrees(110));
        BoundingArea boundingArea = BoundingArea.at(northEast, southWest);

        Point point1 = Point.at(Coordinate.fromDegrees(60), Coordinate.fromDegrees(120));
        Assert.assertTrue(boundingArea.contains(point1));

        Point point2 = Point.at(Coordinate.fromDegrees(45), Coordinate.fromDegrees(120));
        Assert.assertFalse(boundingArea.contains(point2));
    }
}