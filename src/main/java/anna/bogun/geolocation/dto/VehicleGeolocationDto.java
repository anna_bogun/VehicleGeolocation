package anna.bogun.geolocation.dto;

import java.util.Objects;

public class VehicleGeolocationDto {
    private String vehicleName;
    private Coordinates coordinates;

    public VehicleGeolocationDto() {
    }

    public VehicleGeolocationDto(String vehicleName, Coordinates coordinates) {
        this.vehicleName = vehicleName;
        this.coordinates = coordinates;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehicleGeolocationDto that = (VehicleGeolocationDto) o;
        return Objects.equals(vehicleName, that.vehicleName) &&
                Objects.equals(coordinates, that.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vehicleName, coordinates);
    }

    @Override
    public String toString() {
        return "VehicleGeolocationDto{" +
                "vehicleName='" + vehicleName + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }
}

