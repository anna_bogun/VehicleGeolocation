package anna.bogun.geolocation.dto;

import java.util.Objects;

public class RectangleAreaDto {
    private Coordinates northEast;
    private Coordinates southWest;

    public RectangleAreaDto() {
    }

    public RectangleAreaDto(Coordinates northEast, Coordinates southWest) {
        this.northEast = northEast;
        this.southWest = southWest;
    }

    public Coordinates getNorthEast() {
        return northEast;
    }

    public void setNorthEast(Coordinates northEast) {
        this.northEast = northEast;
    }

    public Coordinates getSouthWest() {
        return southWest;
    }

    public void setSouthWest(Coordinates southWest) {
        this.southWest = southWest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RectangleAreaDto that = (RectangleAreaDto) o;
        return Objects.equals(northEast, that.northEast) &&
                Objects.equals(southWest, that.southWest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(northEast, southWest);
    }

    @Override
    public String toString() {
        return "RectangleAreaDto{" +
                "northEast=" + northEast +
                ", southWest=" + southWest +
                '}';
    }
}
