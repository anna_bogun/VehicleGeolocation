package anna.bogun.geolocation.repository;

import anna.bogun.geolocation.model.VehicleGeolocation;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleGeolocationRepository
        extends ReactiveCrudRepository<VehicleGeolocation, String> {
}
