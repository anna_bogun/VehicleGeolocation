package anna.bogun.geolocation.service;


import anna.bogun.geolocation.dto.RectangleAreaDto;
import anna.bogun.geolocation.dto.VehicleGeolocationDto;
import anna.bogun.geolocation.model.VehicleGeolocation;
import anna.bogun.geolocation.repository.VehicleGeolocationRepository;
import com.grum.geocalc.BoundingArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class VehicleGeolocationService {
    private VehicleGeolocationRepository vehicleGeolocationRepository;

    @Autowired
    public VehicleGeolocationService(VehicleGeolocationRepository vehicleGeolocationRepository) {
        this.vehicleGeolocationRepository = vehicleGeolocationRepository;
    }

    public void saveVehicleGeolocation(VehicleGeolocationDto vehicleGeolocation) {
        vehicleGeolocationRepository
                .save(new VehicleGeolocation(vehicleGeolocation.getVehicleName(),
                        vehicleGeolocation.getCoordinates()))
                .subscribe();

    }

    public Flux<VehicleGeolocationDto> getVehicleGeolocationsInRectangle(RectangleAreaDto rectangleAreaDto) {
        BoundingArea boundingArea = BoundingArea.at(rectangleAreaDto.getNorthEast().extractPoint(),
                rectangleAreaDto.getSouthWest().extractPoint());
        return vehicleGeolocationRepository
                .findAll()
                .filter(entity ->
                        boundingArea.contains(entity.getCoordinates().extractPoint()
                        ))
                .map(entity ->
                        new VehicleGeolocationDto(entity.getVehicleName(), entity.getCoordinates()
                        ));
    }

    public Flux<VehicleGeolocationDto> getAll() {
        return vehicleGeolocationRepository
                .findAll()
                .map(entity ->
                        new VehicleGeolocationDto(entity.getVehicleName(), entity.getCoordinates()
                        ));
    }
}
