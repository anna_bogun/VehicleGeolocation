package anna.bogun.geolocation.model;

import anna.bogun.geolocation.dto.Coordinates;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.Objects;
import java.util.UUID;

@Document(collection = "geolocation")
public class VehicleGeolocation {
    @Id
    private String id;
    @Indexed(unique = true)
    private String vehicleName;
    private Coordinates coordinates;

    public VehicleGeolocation() {
    }

    public VehicleGeolocation(String id, String vehicleName, Coordinates coordinates) {
        this.id = id;
        this.vehicleName = vehicleName;
        this.coordinates = coordinates;
    }

    public VehicleGeolocation(String vehicleName, Coordinates coordinates) {
        this.id = UUID.randomUUID().toString();
        this.vehicleName = vehicleName;
        this.coordinates = coordinates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehicleGeolocation that = (VehicleGeolocation) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(vehicleName, that.vehicleName) &&
                Objects.equals(coordinates, that.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, vehicleName, coordinates);
    }

    @Override
    public String toString() {
        return "VehicleGeolocation{" +
                "id='" + id + '\'' +
                ", vehicleName='" + vehicleName + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }
}
