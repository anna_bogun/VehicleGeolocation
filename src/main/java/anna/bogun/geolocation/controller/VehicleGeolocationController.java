package anna.bogun.geolocation.controller;

import anna.bogun.geolocation.dto.RectangleAreaDto;
import anna.bogun.geolocation.dto.VehicleGeolocationDto;
import anna.bogun.geolocation.service.VehicleGeolocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/geolocation")
public class VehicleGeolocationController {
    private VehicleGeolocationService vehicleGeolocationService;

    @Autowired
    public VehicleGeolocationController(VehicleGeolocationService vehicleGeolocationService) {
        this.vehicleGeolocationService = vehicleGeolocationService;
    }

    @PostMapping
    public void saveVehicleGeolocation(@RequestBody VehicleGeolocationDto vehicleGeolocation) {
        vehicleGeolocationService.saveVehicleGeolocation(vehicleGeolocation);
    }


    @PutMapping("in-area")
    public Flux<VehicleGeolocationDto> getVehicleGeolocationsInRectangle(@RequestBody RectangleAreaDto rectangleAreaDto) {
        return vehicleGeolocationService.getVehicleGeolocationsInRectangle(rectangleAreaDto);
    }

    @GetMapping
    public Flux<VehicleGeolocationDto> getAll() {
        return vehicleGeolocationService.getAll();
    }
}
